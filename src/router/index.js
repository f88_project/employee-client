import { createRouter, createWebHistory } from 'vue-router';
import AppLayout from '@/layout/AppLayout.vue';

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/',
            component: AppLayout,
            children: [
                {
                    path: '/',
                    name: 'User',
                    component: () => import('@/views/users/user.vue')
                },
                {
                    path: '/department',
                    name: 'Department',
                    component: () => import('@/views/departments/department.vue')
                }
            ]
        },
        {
            path: '/auth/login',
            name: 'login',
            component: () => import('@/views/auth/Login.vue')
        },
        {
            path: '/auth/access',
            name: 'access',
            component: () => import('@/views/auth/Access.vue')
        }
    ]
});

export default router;
