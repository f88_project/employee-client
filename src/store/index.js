import { createStore } from 'vuex';
import user from './modules/user';
import department from './modules/department';

const modules = {
    user,
    department
};

export const store = createStore({
    modules
});
