/* eslint-disable no-unused-vars */
import userApi from '../../services/user.api';
import roleApi from '../../services/role.api';

const state = {
    users: [],
    roles: [],
    metadata: {},
    permissions: [],
    userEmpty: {}
};

const getters = {
    users: (state) => state.users,
    roles: (state) => state.roles,
    metadata: (state) => state.metadata,
    userEmpty: (state) => state.userEmpty
};

const actions = {
    GET_USERS({ commit }, payload) {
        userApi.getPaging(payload).then((res) => {
            commit('SET_USERS', res.data.items);
            commit('SET_METADATA', res.data.metaData);
        });
    },
    async CREATE_USER({ commit }, payload) {
        await userApi.create(payload);
    },

    async UPDATE_USER({ commit }, payload) {
        await userApi.edit(payload);
    },
    async GET_ALL_ROLE() {
        await roleApi.getAll();
    },
    async DELETE_USER({ commit }, payload) {
        await userApi.delete(payload);
    },
    async ADD_ROLE({ commit }, payload) {
        await userApi.addRole(payload);
    }
};

const mutations = {
    SET_USERS(state, users) {
        state.users = users;
    },

    SET_METADATA(state, metadata) {
        state.metadata = metadata;
    },

    SET_ROLES(state, roles) {
        state.roles = roles;
    },
    SET_USER_EMPTY(state, user) {
        state.userEmpty = user;
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};
