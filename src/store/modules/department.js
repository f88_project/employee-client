/* eslint-disable no-unused-vars */
import departmentApi from '../../services/department.api';

const state = {
    departmentAll: []
};

const getters = {
    departmentAll: (state) => state.departmentAll
};

const actions = {
    async GET_ALL_DEPARTMENT({ commit }) {
        await departmentApi.getAll().then((res) => {
            commit('SET_DEPARTMENT_ALL', res.data);
        });
    }
};

const mutations = {
    SET_DEPARTMENT_ALL(state, departments) {
        state.departmentAll = departments;
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};
