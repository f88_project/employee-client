import http from './http';

const userApi = {
    getPaging({ pageIndex, pageSize, search }) {
        return http.get('/api/users', {
            params: {
                pageIndex,
                pageSize,
                search
            }
        });
    },

    getById(id) {
        return http.get('/api/users/' + id);
    },

    create(request) {
        return http.post('/api/users', request);
    },

    edit(request) {
        return http.put('/api/users/' + request.id, request);
    },

    delete(id) {
        return http.delete('/api/users/' + id);
    },

    addRole(id, roleId) {
        return http.post('/api/users/role/' + id, { roleIds: [roleId] });
    }
};

export default userApi;
