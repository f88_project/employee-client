import http from './http';

const roleApi = {
    getAll() {
        return http.get('/api/roles/all');
    }
};

export default roleApi;
