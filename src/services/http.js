import axios, { HttpStatusCode } from 'axios';
import { getAccessTokenFromLS, setAccessTokenToLS, getRefreshTokenFromLS, setRefreshTokenToLS, setProfileToLS, parseJwt } from '../utils/utils';
import path from '../contants/path';

class Http {
    instance;
    accessToken;
    constructor() {
        this.accessToken = getAccessTokenFromLS();
        this.refreshToken = getRefreshTokenFromLS();
        this.instance = axios.create({
            baseURL: import.meta.env.VITE_API_URL,
            timeout: 10000,
            headers: {
                'Content-Type': 'application/json'
            }
        });

        this.instance.interceptors.request.use(
            (config) => {
                if (this.accessToken && config.headers) {
                    config.headers.Authorization = 'bearer ' + this.accessToken;
                    return config;
                }
                return config;
            },
            (error) => {
                return Promise.reject(error);
            }
        );

        this.instance.interceptors.response.use(
            (response) => {
                const { url } = response.config;
                if (url === path.login || url === path.register || url === path.externalLogin) {
                    const data = response.data;
                    this.accessToken = data.access_token;
                    setAccessTokenToLS(this.accessToken);
                    setRefreshTokenToLS(data.refresh_token);
                    setProfileToLS(parseJwt(this.accessToken));
                }
                return response;
            },
            (error) => {
                const config = error.response?.config || { headers: {}, url: '' };
                const { url } = config;
                if (error.response?.status === HttpStatusCode.Unauthorized && url !== path.refreshToken) {
                    this.refreshTokenRequest = this.refreshTokenRequest
                        ? this.refreshTokenRequest
                        : this.handleRefreshToken().finally(() => {
                              // Giữ refreshTokenRequest trong 10s cho những request tiếp theo nếu có 401 thì dùng
                              setTimeout(() => {
                                  this.refreshTokenRequest = null;
                              }, 10000);
                          });
                    return this.refreshTokenRequest.then((access_token) => {
                        // Nghĩa là chúng ta tiếp tục gọi lại request cũ vừa bị lỗi
                        return this.instance({ ...config, headers: { ...config.headers, authorization: access_token } });
                    });
                }

                if (error.response?.status === HttpStatusCode.Unauthorized && url === path.refreshToken) {
                    window.location.href = path.login;
                }
                if (error.response?.status === HttpStatusCode.Forbidden) {
                    window.location.href = path.access;
                }
                return Promise.reject(error);
            }
        );
    }

    async handleRefreshToken() {
        try {
            const res = await this.instance.post(path.refreshToken, {
                refreshToken: this.refreshToken
            });
            const { access_token, refresh_token } = res.data;
            setAccessTokenToLS(access_token);
            setRefreshTokenToLS(refresh_token);
            this.accessToken = access_token;
            this.refreshToken = refresh_token;
            return access_token;
        } catch (error) {
            this.accessToken = '';
            this.refreshToken = '';
            throw error;
        }
    }
}

const http = new Http().instance;

export default http;
