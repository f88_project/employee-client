import http from './http';

const departmentApi = {
    getAll() {
        return http.get('/api/departments/all');
    },

    getPaging(pageIndex, pageSize, search) {
        return http.get('/api/departments', {
            params: {
                pageIndex,
                pageSize,
                search
            }
        });
    },

    getById(id) {
        return http.get('/api/departments/' + id);
    },

    create(request) {
        return http.post('/api/departments', request);
    },

    edit(request) {
        return http.put('/api/departments/' + request.id, request);
    },

    delete(id) {
        return http.delete('/api/departments/' + id);
    }
};

export default departmentApi;
