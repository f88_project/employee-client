import http from './http';

const authApi = {
    login(email, password) {
        return http.post('/auth/login', { userName: email, password });
    },

    refreshToken(refreshToken) {
        return http.post('/auth/refresh-token', { refreshToken });
    }
};

export default authApi;
