const path = {
    home: '/',
    login: '/auth/login',
    access: '/auth/access',
    refreshToken: '/auth/refresh-token'
};

export default path;
