import moment from 'moment';

export function parseJwt(token) {
    try {
        return JSON.parse(atob(token.split('.')[1]));
    } catch (e) {
        return null;
    }
}

export const setAccessTokenToLS = (access_token) => {
    if (typeof window !== 'undefined') {
        localStorage.setItem('access_token', access_token);
    }
};

export const setProfileToLS = (profile) => {
    localStorage.setItem('profile', JSON.stringify(profile));
};

export const setRefreshTokenToLS = (refresh_token) => {
    localStorage.setItem('refresh_token', refresh_token);
};

export const clearLS = () => {
    localStorage.removeItem('access_token');
    localStorage.removeItem('refresh_token');
    localStorage.removeItem('profile');
};

export const getAccessTokenFromLS = () => {
    return localStorage.getItem('access_token') || '';
};

export const getRefreshTokenFromLS = () => {
    return localStorage.getItem('refresh_token') || '';
};

export const getProfileFromLS = () => {
    const result = localStorage.getItem('profile');
    return result ? JSON.parse(result) : null;
};

export const validateEmail = (email) => {
    return String(email)
        .toLowerCase()
        .match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
};

export const validatePhoneNumber = (phone) => {
    const regexPhoneNumber = /(84|0[3|5|7|8|9])+([0-9]{8})\b/g;
    return phone.match(regexPhoneNumber) ? true : false;
};

export const renderDate = (date) => {
    return moment(date).format('DD/MM/YYYY');
};
